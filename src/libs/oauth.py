from flask import g
from flask_oauthlib.client import OAuth

from libs.config import getconf

oauth = OAuth()

github = oauth.remote_app(
	"github",
	consumer_key=getconf("oauth", "github", "consumer_key"),
	consumer_secret=getconf("oauth", "github", "consumer_secret"),
	request_token_params={"scope": "read:user,user:email"},
	base_url="https://api.github.com",
	request_token_url=None,  # OAuth 2.0, for 1.0 this needs to be set
	access_token_method="POST",
	access_token_url="https://github.com/login/oauth/access_token",
	authorize_url="https://github.com/login/oauth/authorize"
)


@github.tokengetter
def get_github_token():
	if "access_token" in g:
		return g.access_token
