"""
libs.lang

By default, uses 'en-gb.json' file inside the 'lang/' top-level directory.

If locale changes on the fly, run `libs.lang.refresh()`.
If default locale changes, change 'app/default_locale' in 'config.json'.
"""

import json
import pathlib

from libs.config import getconf

default_locale = getconf("app", "default_locale")  # Without .json extension
cached_strings = {}


def refresh() -> None:
	"""
	Takes the locale file and loads it into cached_strings.
	:return: None
	"""
	global cached_strings
	filepath = pathlib.Path(__file__).parent.absolute().joinpath("..", "lang", f"{default_locale}.json")

	with open(filepath, "r") as file:
		cached_strings = json.load(file)


def gettext(*argv: str) -> str:
	"""
	Returns the requested string in the language file.
	:param argv: String indexes, e.g. if you want the user/EMAIL_INVALID_ERROR string:
	gettext("user", "EMAIL_INVALID_ERROR")
	:return: str
	"""
	return_value = cached_strings
	for index in argv:
		if not index in return_value:
			return_value = ""
			break
		return_value = return_value[index]

	return return_value


refresh()
