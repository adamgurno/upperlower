"""
lib.upload_helper
"""

import os
from typing import Union

from flask_uploads import UploadSet, IMAGES, DOCUMENTS, ARCHIVES, TEXT, DATA
from werkzeug.datastructures import FileStorage

from libs.parser import parse

EXTENSIONS = IMAGES + DOCUMENTS + ARCHIVES + TEXT + DATA
FILE_SET = UploadSet(name="uploads", extensions=EXTENSIONS)

file_union = Union[str, FileStorage]


def save_file(file: FileStorage, folder: str = None, filename: str = None) -> str:
	"""
	Takes FileStorage and saves it to a folder.
	:return: str
	"""
	return FILE_SET.save(file, folder, filename)


def get_path(filename: str = None, folder: str = None):
	"""
	Take filename and folder and return full path.
	:return: str
	"""
	return FILE_SET.path(filename=filename, folder=folder)


def find(filename: str, folder: str) -> Union[str, None]:
	"""
	Takes a filename and returns a file on any of the accepted formats.
	:return: str if file exists else None
	"""
	for _format in EXTENSIONS:
		file = f"{filename}.{_format}"
		image_path = FILE_SET.path(filename=filename, folder=folder)
		if os.path.isfile(image_path):
			return image_path
	return None


def _retrieve_filename(file: file_union) -> str:
	"""
	Take FileStorage and return the filename if given parameter is a valid existing file.
	Allows our methods to call this with filenames and FileStorages and always gets back a filename.
	:return: str
	"""
	if isinstance(file, FileStorage):
		return file.filename
	return file


def is_filename_safe(file: file_union, extensions: tuple = EXTENSIONS) -> bool:
	"""
	Check regex and return whether the string matches or not.
	:return: bool
	"""
	filename = _retrieve_filename(file)
	allowed_formats = "|".join(extensions)
	return parse(string=filename, schema="filename", allowed_formats=allowed_formats) is not None


def get_basename(file: file_union) -> str:
	"""
	Return full name of file in the path.
	get_basename("some/folder/file.txt") return "file.txt".
	:return: str
	"""
	filename = _retrieve_filename(file)
	return os.path.split(filename)[1]


def get_extension(file: file_union) -> str:
	"""
	Return extension of a file.
	:return: str
	"""
	filename = _retrieve_filename(file)
	return os.path.splitext(filename)[1]
