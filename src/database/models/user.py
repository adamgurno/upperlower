"""
DO NOT DELETE, essential for logging users out and revoking their tokens. Modify and delete only if you know what you
are doing.
"""

from database import db
from . import Model


class UserModel(Model):
	__tablename__ = 'users'

	username = db.Column(db.String(80), unique=True, nullable=False)
	# The password field must be nullable, if OAuth login is implemented!
	password = db.Column(db.String(128), nullable=True, default=None)
	activated = db.Column(db.Integer, default=False)
	confirmation_code = db.Column(db.String(64), unique=True)
	profile_image = db.Column(db.String(128), nullable=True)

	@classmethod
	def find_by_username(cls, username: str) -> "UserModel":
		"""
		Looks up a user by his username.
		:param username: username
		:return: UserModel object
		"""
		return cls.query.filter_by(username=username).first()

	@classmethod
	def find_by_confirmation_code(cls, confirmation_code: str) -> "UserModel":
		"""
		Looks up a user by his activation_code.
		:param activation_code:
		:return: UserModel object
		"""
		return cls.query.filter_by(confirmation_code=confirmation_code).first()
