"""
DO NOT DELETE, essential for logging users out and revoking their tokens. Modify and delete only if you know what you
are doing.
"""

from database import db
from . import Model


class RevokedTokensModel(Model):
	__tablename__ = "revoked_tokens"

	jti = db.Column(db.String(80), nullable=False)

	@classmethod
	def find_by_jti(cls, jti: str) -> "RevokedTokensModel":
		"""
		Looks up a store by his name.
		:param jti: store name
		:return: StoreModel object
		"""
		return cls.query.filter_by(jti=jti).first()
