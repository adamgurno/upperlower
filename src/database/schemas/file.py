from marshmallow import Schema

from .custom_fields import FileStorageField


class FileSchema(Schema):
	file = FileStorageField(required=True)
