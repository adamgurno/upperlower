from marshmallow_sqlalchemy import fields

from database.models.payment import PaymentModel
from . import ma, dump_only_preset
from .item import ItemSchema


class PaymentSchema(ma.ModelSchema):
	class Meta:
		model = PaymentModel

		load_only = ("token",)  # Never dump.
		dump_only = dump_only_preset + ("status",)  # Never require on input data.
		include_fk = True  # Include foreign keys.

	items = fields.Nested(ItemSchema, many=True)
