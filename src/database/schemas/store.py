from database.models.store import StoreModel
from . import ma, dump_only_preset
from .item import ItemSchema


class StoreSchema(ma.ModelSchema):
	items = ma.Nested(ItemSchema, many=True)

	class Meta:
		model = StoreModel

		load_only = ()  # Never dump.
		dump_only = dump_only_preset  # Never require on input data.
		include_fk = True  # Include foreign keys.
