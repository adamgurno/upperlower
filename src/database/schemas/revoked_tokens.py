from database.models.revoked_tokens import RevokedTokensModel
from . import ma, dump_only_preset


class RevokedTokensSchema(ma.ModelSchema):
	class Meta:
		model = RevokedTokensModel

		load_only = ("jti",)  # Never dump.
		dump_only = dump_only_preset  # Never require on input data.
