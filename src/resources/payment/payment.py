from collections import Counter

from flask import request
from flask_restful import Resource
from stripe import error

from database.models.item import ItemModel
from database.models.payment import ItemsInPayment
from database.models.payment import PaymentModel
from libs.lang import gettext
from . import payment_schema, multiple_payment_schema


class Payment(Resource):
	@classmethod
	def get(cls):
		return {"payments": multiple_payment_schema.dump(PaymentModel.find_all())}, 200

	@classmethod
	def post(cls):
		"""
		Expect a token and a list of item ids from the request body.
		Construct a payment and talk to the Stripe API to make a charge.
		:return:
		"""
		data = request.get_json()
		items = []
		item_id_quantities = Counter(data["item_ids"])

		for _id, count in item_id_quantities.most_common():
			item = ItemModel.find_by_id(_id)
			if not item:
				return {"message": gettext("payment", "ITEM_NOT_FOUND_ERROR").format(_id),
				        "error": "ITEM_NOT_FOUND_ERROR"}, 404
			items.append(ItemsInPayment(item_id=_id, quantity=count, price=item.price))

		payment = PaymentModel(items=items, status="pending")
		payment.save()  # Does not submit to stripe, just keeps track.

		try:
			payment.set_status("failed")  # assume the order would fail until it's completed
			payment.charge_with_stripe(data["token"])
			payment.set_status("complete")  # charge succeeded
			return payment_schema.dump(payment), 200
		except error.CardError as e:
			# Since it's a decline, stripe.error.CardError will be caught
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except error.RateLimitError as e:
			# Too many requests made to the API too quickly
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except error.InvalidRequestError as e:
			# Invalid parameters were supplied to Stripe's API
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except error.AuthenticationError as e:
			# Authentication with Stripe's API failed
			# (maybe you changed API keys recently)
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except error.APIConnectionError as e:
			# Network communication with Stripe failed
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except error.StripeError as e:
			# Display a very generic error to the user, and maybe send
			# yourself an email
			return {"error": "STRIPE_ERROR", "message": gettext("payment", "STRIPE_ERROR"),
			        "more": e.json_body}, e.http_status
		except Exception as e:
			# Something else happened, completely unrelated to Stripe
			print(e)
			return {"message": gettext("_app", "UNDEFINED_ERROR"), "error": "UNDEFINED_ERROR"}, 500
