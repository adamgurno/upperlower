from database.schemas.payment import PaymentSchema

payment_schema = PaymentSchema()
multiple_payment_schema = PaymentSchema(many=True)
