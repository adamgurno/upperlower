from flask_restful import Resource

from database.models.store import StoreModel
from . import store_list_schema


class StoreList(Resource):
	@classmethod
	def get(cls):
		"""
		Retrieve all entities.
		:return:
		"""
		return {
			"stores": store_list_schema.dump(StoreModel.find_all())
		}
