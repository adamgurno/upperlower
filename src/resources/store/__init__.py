from database.schemas.store import StoreSchema

store_schema = StoreSchema()
store_list_schema = StoreSchema(many=True)
