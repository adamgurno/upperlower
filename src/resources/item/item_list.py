from flask_jwt_extended import jwt_optional, get_jwt_identity
from flask_restful import Resource

from database.models.item import ItemModel
from . import item_list_schema


class ItemList(Resource):
	@classmethod
	@jwt_optional
	def get(cls):
		"""
		Retrieve all entities.
		:return:
		"""
		items = ItemModel.find_all()
		if get_jwt_identity():
			return {
				       "items": item_list_schema.dump(items)
			       }, 200
		return {
			       "items": [item.name for item in items]
		       }, 200
