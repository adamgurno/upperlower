"""
Not essential for the core of the app (users, tokens, ...) to work. Feel free to modify & delete. Don't forget to also
remove the model and schema.
"""
from flask import request
from flask_jwt_extended import jwt_required, fresh_jwt_required
from flask_restful import Resource

from database.models.item import ItemModel
from database.models.store import StoreModel
from libs.lang import gettext
from . import item_schema


class Item(Resource):
	@classmethod
	def get(cls, name: str):
		"""
		Retrieve an entity.
		:param name: Item name
		:return:
		"""
		item = ItemModel.find_by_name(name)
		if item:
			return item_schema.dump(item), 200
		return {
			       "message": gettext("item", "ITEM_NOT_FOUND_ERROR").format(name),
			       "error": "NOT_FOUND"
		       }, 404

	@classmethod
	@jwt_required
	def post(cls, name: str):
		"""
		Create an entity.
		:param name: Item name
		:return:
		"""
		if ItemModel.find_by_name(name):
			return {
				       "message": gettext("item", "ITEM_EXISTS_ERROR").format(name),
				       "error": "ALREADY_EXISTS"
			       }, 400

		item_json = request.get_json()  # price, store_id
		item_json["name"] = name  # add name to json

		item = item_schema.load(item_json)

		if not StoreModel.find_by_id(item_json["store_id"]):
			return {
				       "message": gettext("item", "STORE_DOES_NOT_EXIST_ERROR"),
				       "error": "STORE_DOES_NOT_EXIST"
			       }, 400

		try:
			item.save()
		except:
			return {
				       "message": gettext("_app", "UNDEFINED_ERROR"),
				       "error": "UNDEFINED_ERROR"
			       }, 500

		return item_schema.dump(item), 201

	@classmethod
	@fresh_jwt_required
	def delete(cls, name: str):
		"""
		Delete an entity.
		:param name: Item name
		:return:
		"""
		item = ItemModel.find_by_name(name)
		if item:
			item.delete()
			return {
				       "message": gettext("item", "ITEM_DELETED")
			       }, 200
		return {
			       "message": gettext("item", "ITEM_NOT_FOUND_ERROR").format(name),
			       "error": "NOT_FOUND"
		       }, 404

	@classmethod
	@jwt_required
	def put(cls, name: str):
		"""
		Change an entity.
		:param name: Item name
		:return:
		"""
		item_json = request.get_json()
		item = ItemModel.find_by_name(name)

		item_json["name"] = name  # add name to json

		if item:
			item.price = item_json["price"] if "price" in item_json else None
			item.store_id = item_json["store_id"] if "store_id" in item_json else None
			item_schema.load(item_json)
		else:
			item = item_schema.load(item_json)

		if not StoreModel.find_by_id(item.store_id):
			return {
				       "message": gettext("item", "STORE_DOES_NOT_EXIST_ERROR"),
				       "error": "STORE_DOES_NOT_EXIST"
			       }, 400

		try:
			item.save()
		except:
			return {
				       "message": gettext("_app", "UNDEFINED_ERROR"),
				       "error": "UNDEFINED_ERROR"
			       }, 500

		return item_schema.dump(item), 201
