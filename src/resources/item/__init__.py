from database.schemas.item import ItemSchema

item_schema = ItemSchema()
item_list_schema = ItemSchema(many=True)
