from database.schemas.file import FileSchema
from database.schemas.revoked_tokens import RevokedTokensSchema
from database.schemas.user import UserSchema

user_schema = UserSchema()
revoked_token_schema = RevokedTokensSchema()
file_schema = FileSchema()
