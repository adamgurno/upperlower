from flask import request
from flask_jwt_extended import fresh_jwt_required, get_jwt_identity
from flask_restful import Resource
from werkzeug.security import generate_password_hash, check_password_hash

from database.models.user import UserModel
from libs.lang import gettext
from . import user_schema


class ChangePassword(Resource):
	@classmethod
	@fresh_jwt_required
	def post(cls):
		data = request.get_json()

		if "new_password" not in data:
			return {
				       "message": gettext("_app", "BAD_SYNTAX_ERROR").format("The new_password field is required."),
				       "error": "BAD_SYNTAX_ERROR"
			       }, 400

		new_password = data["new_password"]
		del (data["new_password"])

		user_data = user_schema.load(data)
		user = UserModel.find_by_username(user_data.username)

		if not user:
			if not user:
				return {
					       "message": gettext("user", "USER_NOT_FOUND_ERROR"),
					       "error": "NOT_FOUND"
				       }, 404

		if user.id != get_jwt_identity():
			return {
				       "message": gettext("_app", "UNAUTHORIZED_ERROR").format(
					       "you can not change a different users password"),
				       "error": "UNAUTHORIZED_ERROR"
			       }, 403

		if not check_password_hash(pwhash=user.password, password=user_data.password):
			return {
				       "message": gettext("user", "INVALID_CREDENTIALS_ERROR"),
				       "error": "INVALID_CREDENTIALS_ERROR"
			       }, 400

		user.password = generate_password_hash(password=new_password)
		user.save()

		return {"message": gettext("user", "PASSWORD_CHANGED")}, 201
