from flask import request
from flask_restful import Resource
from werkzeug.security import generate_password_hash

from database.models.user import UserModel
from libs.lang import gettext
from libs.random import random_string
from . import user_schema


class UserRegister(Resource):
	@classmethod
	def post(cls):
		"""
		Create an entity.
		:return:
		"""
		user = user_schema.load(request.get_json())  # try...except not necessary, exception is being handled in app.py

		if UserModel.find_by_username(user.username):
			return {
				       "message": gettext("user", "USER_EXISTS_ERROR"),
				       "error": "ALREADY_EXISTS"
			       }, 400

		user.password = generate_password_hash(password=user.password)
		user.confirmation_code = random_string(64)

		# Create new codes until a unique code has been generated.
		while UserModel.find_by_confirmation_code(user.confirmation_code):
			user.confirmation_code = random_string(64)

		# TODO: Send an email to the user and stop dumping the activation code with the response?

		try:
			user.save()
		except:
			return {
				       "message": gettext("_app", "UNDEFINED_ERROR"),
				       "error": "UNDEFINED_ERROR"
			       }, 500

		return {
			       "message": gettext("user", "USER_CREATED"),
			       "confirmation_code": user.confirmation_code,
			       "username": user.username,
			       "WARNING": "This code should be sent to the user by mail or similar! Fix this before going to production."
		       }, 201
