"""
DO NOT DELETE, essential for logging users out and revoking their tokens. Modify and delete only if you know what you
are doing.
"""
from flask_jwt_extended import jwt_required, get_jwt_identity, \
	fresh_jwt_required, get_raw_jwt
from flask_restful import Resource

from database.models.user import UserModel
from libs.lang import gettext
from . import user_schema, revoked_token_schema


class User(Resource):
	@classmethod
	@jwt_required
	def get(cls, user_id: int):
		"""
		Retrieve an entity.
		:return:
		"""
		user = UserModel.find_by_id(user_id)
		if not user:
			return {
				       "message": gettext("user", "USER_NOT_FOUND_ERROR"),
				       "error": "NOT_FOUND"
			       }, 404

		return user_schema.dump(user)

	@classmethod
	@fresh_jwt_required
	def delete(cls, user_id: int):
		"""
		Delete an entity.
		:return:
		"""
		user = UserModel.find_by_id(user_id)
		if not user:
			return {
				       "message": gettext("user", "USER_NOT_FOUND_ERROR"),
				       "error": "NOT_FOUND"
			       }, 404

		# Blacklist the users JTI if user deletes himself.
		if get_jwt_identity() == user.id:
			jti = get_raw_jwt()['jti']
			revoked_jti = revoked_token_schema.load({"jti": jti})

			try:
				revoked_jti.save()
			except:
				return {
					       "message": gettext("_app", "UNDEFINED_ERROR"),
					       "error": "UNDEFINED_ERROR"
				       }, 500

		user.delete()
		return {
			       "message": gettext("user", "USER_DELETED")
		       }, 200
