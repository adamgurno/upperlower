from flask_jwt_extended import jwt_refresh_token_required, get_jwt_identity, create_access_token
from flask_restful import Resource


class TokenRefresh(Resource):
	@classmethod
	@jwt_refresh_token_required
	def post(cls):
		"""
		Refreshes the access token. Requires the refresh token as authentication method.
		:return:
		"""
		current_user = get_jwt_identity()
		new_access_token = create_access_token(identity=current_user, fresh=False)

		return {
			       "access_token": new_access_token
		       }, 200
