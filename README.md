# flask-api-example

Example code base for Flask API apps. Can be used as base structure for new projects. Contains multiple examples (such as database relationships, user authentication, ...), custom libraries (config parsing, random string generator, upload helpers, language parser, ...) as well as a stripe integration and third party login with OAuth.

Built with Flask, Flask-Restful and Flask-SQLAlchemy. Uses JWT for authorization.

## Installation

### Prerequisites

You need to have **Python >= 3.7** installed. Also make sure you have Python **pip** installed.

To install your environment you need to clone to repository and switch into the install directory:

```bash
$ git clone https://gitlab.com/floriantraun/flask-api-example.git
$ cd flask-api-example/src/
```

Afterwards install and create the virtual environment and install all requirements:

```bash
$ pip install virtualenv
$ virtualenv venv # Alternatively: python3.7 -m virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

You may choose the install a MySQL database as well, but it is possible to store everything in a SQLite database.

If you are done setting up the virtual environment, go ahead and copy `config/config.json.example` to `config/config.json` (don't forget to set your `app_secret`).

### Setup

Run the migrations with `flask db upgrade --directory database/migrations/`.

#### Development

You are all done! Start coding or start the server with `python app.py`.

If you make changes to the database schema, you can create a new migration by running `flask db migrate --directory database/migrations/`.

#### Production

It is recommended that you do not launch the development server by calling `app.py` but rather use a production WSGI server instead (nginx, uWSGI, ...).

Also you may secure your API by using a service like [Cloudflare](https://cloudflare.com).

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](LICENSE.md)

This project is licensed under the MIT license (see [LICENSE.md](LICENSE.md)).
